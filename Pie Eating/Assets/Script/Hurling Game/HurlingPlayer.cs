using Script.BaseGame;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

public class HurlingPlayer : PlayerBase
{
    [SerializeField] private TextMeshProUGUI m_ScoreTextMesh;
    [SerializeField] private HurlingPie m_Pie;
    [SerializeField] private TextMeshProUGUI m_AttemptsTextMesh;

    [HideInInspector]public int Attempts;
    
    private float m_PositiveTime;
    private float m_NegativeTime;
    private bool m_SetButton;
    
    private void Start()
    {
        m_AttemptsTextMesh.text = Attempts.ToString("00");
    }

    protected override void PositiveButton()
    {
        if (!m_SetButton || !GameStates.gameActive || m_Pie.active) return;
        m_SetButton = false;
        m_PositiveTime = Time.time;
        float speed = m_PositiveTime - m_NegativeTime;
        m_Pie.HurlPie(speed);
    }

    protected override void NegativeButton()
    {
        if (m_SetButton || !GameStates.gameActive || m_Pie.active) return;
        m_SetButton = true;
        m_NegativeTime = Time.time;
    }

    public override void SetData(CharacterData player)
    {
        base.SetData(player);
        m_Pie.SetHurlingImage(player.Sprite);
    }

    public void AddScore()
    {
        m_Score++;
        m_ScoreTextMesh.text = m_Score.ToString("00");
        Scores.AddScore(m_PlayerCharacter, m_Score);
    }

    public void AdjustAttempts(int adjustment)
    {
        Attempts += adjustment;
        m_AttemptsTextMesh.text = Attempts.ToString("00");
        if (Attempts <= 0)
        {
            m_Pie.AttemptsComplete();
            HurlingPlayerController.playerCompletedAttempts?.Invoke(m_PlayerCharacter.Index);
        }
        else
        {
            m_Pie.ResetTarget();
        }
    }
}