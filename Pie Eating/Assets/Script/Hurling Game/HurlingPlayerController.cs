using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HurlingPlayerController : PlayerController
{
    [Space]
    [SerializeField] private List<int> targetPositions = new List<int>();
    
    [Space]
    [SerializeField] private int additionalAttempts;
    
    private Dictionary<int, bool> _playersCompleted;
    
    public static Action<int> playerCompletedAttempts;

    public static Func<int, int> getTargetPosition;

    public static int maxDistance;
    
    private void OnEnable()
    {
        playerCompletedAttempts += PlayerComplete;
        getTargetPosition += ReturnTargetPosition;
    }

    private void OnDisable()
    {
        playerCompletedAttempts = null;
        getTargetPosition = null;
    }

    public override void Initialise()
    {
        base.Initialise();
        SetPlayerData();
        FindMaxDistance();
    }

    private void SetPlayerData()
    {
        _playersCompleted = new Dictionary<int, bool>();
        foreach (var playerBase in m_PlayerUI)
        {
            _playersCompleted.Add(playerBase.m_PlayerCharacter.Index, false);
            var player = (HurlingPlayer)playerBase;
            player.Attempts = targetPositions.Count + additionalAttempts;
        }
    }
    
    private void FindMaxDistance()
    {
        foreach (var positions in targetPositions.Where(positions => positions > maxDistance))
        {
            maxDistance = positions;
        }
    }

    private void PlayerComplete(int index)
    {
        if (!_playersCompleted.ContainsKey(index))
        {
            Debug.LogError($"missing player index {index}");
            return;
        }

        _playersCompleted[index] = true;
        if (!AllComplete()) return;
        GameStates.EndGame?.Invoke();
    }

    private bool AllComplete()
    {
        return _playersCompleted.All(playerBase => playerBase.Value != false);
    }

    private int ReturnTargetPosition(int index)
    {
        return index > targetPositions.Count ? 0 : targetPositions[index];
    }
}