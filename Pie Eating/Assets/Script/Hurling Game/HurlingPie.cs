using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HurlingPie : MonoBehaviour
{
   [SerializeField] private HurlingPlayer player;
   
   [Header("Required Objects")]
   [SerializeField] private RectTransform targetImage;
   [SerializeField] private Image hurlImage;
   
   [Space]
   [SerializeField] private float sensitivity;

   [Space]
   [Header("Tween")]
   [SerializeField] private float duration;
   [SerializeField] private Ease ease;

   [HideInInspector]public bool active;
   private float _distance;
   private int _maxDistance;
   private int _currentTarget;
   private float _anchoredDistance;
   private RectTransform _hurlingRect;

   private void Start()
   {
      _hurlingRect = hurlImage.GetComponent<RectTransform>();
      _maxDistance = HurlingPlayerController.maxDistance;
      SetTarget();
   }
   
   public void SetHurlingImage(Sprite newHurl)
   {
      hurlImage.sprite = newHurl;
   }
   
   public IEnumerator WaitSetDistance()
   {
      yield return new WaitForSeconds(2);
      ResetHurl();
      SetTarget();
   }

   public void HurlPie(float speed)
   {
      active = true;
      var accuracy = _distance - speed;
      CheckScore(accuracy);
      _hurlingRect.DOAnchorPos(CalculatePosition(accuracy), duration).SetEase(ease).onComplete += () =>
      {
         player.AdjustAttempts(-1);
      };
   }

   private Vector3 CalculatePosition(float accuracy)
   {
      var accuracyPercentage = accuracy / _distance;
      var anchoredPosition = _hurlingRect.anchoredPosition;
      var positionPercentage = _anchoredDistance * accuracyPercentage;
      anchoredPosition.y = Mathf.Abs(_anchoredDistance - positionPercentage);
      return anchoredPosition;
   }

   public void ResetTarget()
   {
      StartCoroutine(nameof(WaitSetDistance));
   }

   public void AttemptsComplete()
   {
      targetImage.gameObject.SetActive(false);
      ResetHurl();
      active = true;
   }
   
   private void SetTarget()
   {
      //distance = Random.Range(minDistance, maxDistance);
      var targetPosition = HurlingPlayerController.getTargetPosition?.Invoke(_currentTarget);
      
      if (targetPosition <= 0 || targetPosition == null)
      {
         targetPosition = Random.Range(1, _maxDistance);
      }
      
      _distance = (float)targetPosition;
      var imageAnchoredPosition = targetImage.anchoredPosition;
      var distancePercentage = _distance / _maxDistance;
      _anchoredDistance = 700 * distancePercentage;
      imageAnchoredPosition.y = _anchoredDistance;
      targetImage.anchoredPosition = imageAnchoredPosition;
      active = false;
   }

   private void ResetHurl()
   {
      var anchoredPosition = _hurlingRect.anchoredPosition;
      anchoredPosition.y = 0;
      _hurlingRect.anchoredPosition = anchoredPosition;
   }

   private void CheckScore(float accuracy)
   {
      if (accuracy > sensitivity || -sensitivity > accuracy) return;
      player.AddScore();
      _currentTarget++;
   }
}
