using System.Collections;
using MiniGame;
using UnityEngine;

namespace Tutorials
{
    public class RuleController : MonoBehaviour
    {
        [SerializeField] private TitleTextMesh m_RulesTitle;
        [SerializeField] private GameRulesObject m_StandardRules;
        [SerializeField] private RuleUI[] m_RulesUI;
        [SerializeField] private int m_WaitTimeBetweenSetOfRules = 5;

        private bool m_GameRulesPlayed;
        private int m_CurrentRule;

        private GameDataObject m_Game;
        
        private void Awake()
        {
            ResetRules();
            m_Game = SelectedGameController.SelectedGame?.Invoke();
        }

        private void Start()
        {
            SetStandardRules();
        }

        private void ResetRules()
        {
            foreach (var rule in m_RulesUI)
            {
                rule.gameObject.SetActive(false);
            }
        }

        private void SetStandardRules()
        {
            if (m_StandardRules == null)
            {
                Debug.LogError($"Missing standard Rules");
                return;
            }

            m_RulesTitle.SetDefaultText();
            SetupRuleUI(m_StandardRules);
        }

        private void SetGameRules()
        {
            if (m_Game == null)
            {
                Debug.LogError($"No MiniGame Rules Selected");
                return;
            }

            m_RulesTitle.SetText(m_Game.Title);
            SetupRuleUI(m_Game.Rules);
        }

        private void SetupRuleUI(GameRulesObject rules)
        {
            for (var i = 0; i < rules.Rules.Length; i++)
            {
                var rule = m_RulesUI[i];
                rule.m_TutorialData = rules.Rules[i];
                rule.InitialiseRule();
                rule.gameObject.SetActive(true);
            }

            m_CurrentRule = 0;
            TriggerRule();
        }

        private void TriggerRule()
        {
            m_RulesUI[m_CurrentRule].RuleComplete += OnRuleComplete;
            m_RulesUI[m_CurrentRule].TriggerRule();
        }

        private void OnRuleComplete()
        {
            m_RulesUI[m_CurrentRule].RuleComplete -= OnRuleComplete;
            if (!MoveRuleOn()) return;
            TriggerRule();
        }

        private bool MoveRuleOn()
        {
            if (m_CurrentRule + 1 >= m_RulesUI.Length)
            {
                if (m_GameRulesPlayed)
                {
                    StartCoroutine(nameof(MoveToMiniGame));
                    return false;
                }

                m_GameRulesPlayed = true;
                StartCoroutine(nameof(MoveToGameRules));
                return false;
            }

            m_CurrentRule++;
            return true;
        }

        private IEnumerator MoveToGameRules()
        {
            yield return new WaitForSeconds(m_WaitTimeBetweenSetOfRules);
            ResetRules();
            SetGameRules();
        }

        private IEnumerator MoveToMiniGame()
        {
            yield return new WaitForSeconds(m_WaitTimeBetweenSetOfRules);
            Loader.LoadScene(Loader.Scene.MiniGames);
        }
    }
}