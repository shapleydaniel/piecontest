using UnityEngine;

namespace Tutorials
{
    [CreateAssetMenu(menuName = "Tutorial/Object", fileName = "Tutorial")]
    public class TutorialObject : ScriptableObject
    {
        [SerializeField] private int m_Order;
        [SerializeField] private string m_Title;
        [SerializeField] private string m_Header;
        [SerializeField] private string m_Footer;
        [SerializeField] private Sprite m_Graphic;
        [SerializeField] private AudioClip m_Speech;

        public int Order => m_Order;
        public string Title => m_Title;
        public string Header => m_Header;

        public string Footer => m_Footer;

        public Sprite Graphic => m_Graphic;

        public AudioClip VO => m_Speech;
    }

}