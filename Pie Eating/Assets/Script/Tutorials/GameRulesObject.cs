using UnityEngine;

namespace Tutorials
{
    [CreateAssetMenu(menuName = "Tutorial/GameRules", fileName = "GameRules")]
    public class GameRulesObject : ScriptableObject
    {
        [SerializeField] private MiniGames m_MiniGame;
        [SerializeField] private TutorialObject[] m_Rules;

        public MiniGames MiniGame => m_MiniGame;
        public TutorialObject[] Rules => m_Rules;
    }
}