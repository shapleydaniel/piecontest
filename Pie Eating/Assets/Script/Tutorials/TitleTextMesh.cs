using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TitleTextMesh : MonoBehaviour
{
    [SerializeField] private string defaultText;
    
    private TextMeshProUGUI titleTextMesh;

    private void Awake()
    {
        titleTextMesh = GetComponent<TextMeshProUGUI>();
        if (!string.IsNullOrEmpty(defaultText)) SetDefaultText();
    }

    public void SetText(string text)
    {
        titleTextMesh.text = text;
    }

    public void SetDefaultText()
    {
        SetText(defaultText);
    }
}