using UnityEngine;

public class TutorialDisplay : Display
{
    [SerializeField] private Camera tutorialDisplayCamera;

    public override void Initialise()
    {
        tutorialDisplayCamera.targetDisplay = displayID;
        if(displayID > 0)tutorialDisplayCamera.GetComponent<AudioListener>().enabled = false;
    }
}