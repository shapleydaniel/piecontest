using System;
using DG.Tweening;
using Script.Audio;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Tutorials
{
    public class RuleUI : MonoBehaviour
    {
        [Header("RequiredComponents")]
        [SerializeField] private TextMeshProUGUI m_TitleTextMesh;
        [SerializeField] private TextMeshProUGUI m_HeaderTextMesh;
        [SerializeField] private TextMeshProUGUI m_FooterTextMesh;
        [SerializeField] private Image m_GraphicImage;
    
        [Header("Tween")]
        [SerializeField] private RectTransform groupObject;
        [SerializeField] private float duration;
        [SerializeField] private Ease ease;
        
        [HideInInspector] public TutorialObject m_TutorialData;
        public Action RuleComplete;
    
        public void InitialiseRule()
        {
            if (m_TutorialData == null)
            {
                Debug.LogError($"Missing Rule Data: {gameObject.name}");
                return;
            }
    
            m_TitleTextMesh.text = m_TutorialData.Title;
            m_HeaderTextMesh.text = m_TutorialData.Header;
            m_FooterTextMesh.text = m_TutorialData.Footer;
            m_GraphicImage.sprite = m_TutorialData.Graphic;
            groupObject.anchoredPosition = new Vector2(0, 1100);
        }
    
        public void TriggerRule()
        {
            if (TriggerAudio())
            {
                TriggerGroupTransition();
            }
            else
            {
                TriggerGroupTransition(RuleComplete);
            }
        }
    
    
        private void TriggerGroupTransition(Action OnComplete = null)
        {
            groupObject.DOAnchorPos(Vector2.zero, duration).SetEase(ease).OnComplete(() => OnComplete?.Invoke());
        }
    
        private bool TriggerAudio()
        {
            if (m_TutorialData.VO == default) return false;
            AudioController.TriggerAudio(m_TutorialData.VO, AudioMasters.VO, 1, RuleComplete);
            return true;
        }
    }
}