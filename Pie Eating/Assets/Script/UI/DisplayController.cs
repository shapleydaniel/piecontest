using UnityEngine;

public class DisplayController : MonoBehaviour
{
    [SerializeField] protected Display displayPrefab;

    private void Awake()
    {
        for (var i = 0; i < UnityEngine.Display.displays.Length; i++)
        {
             var gameController = Instantiate(displayPrefab, transform);
             gameController.SetDisplayID = i;
             var o = gameController.gameObject;
             o.name = $"{o.name} {i}";
             gameController.Initialise();
        }
    }
}