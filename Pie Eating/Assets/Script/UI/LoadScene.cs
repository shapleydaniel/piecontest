using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSelectScene : MonoBehaviour
{
    [SerializeField] private string m_SceneName;
    
    public void LoadSelectPlayerScene()
    {
        SceneController.LoadNewScene?.Invoke(m_SceneName);
    }
}