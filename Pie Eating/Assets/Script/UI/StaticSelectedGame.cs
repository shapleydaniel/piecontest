using System;
using Random = UnityEngine.Random;

public enum MiniGames
{
   Eating = 0,
   Reaction = 1,
   Hurling = 2,
   Invasion = 3
}

public static class StaticSelectedGame
{
   private static MiniGames SelectedMiniGame = MiniGames.Reaction;

   public static MiniGames GetSelectedMiniGame => SelectedMiniGame;

   public static MiniGames SelectMiniGame()
   {
      var values = Enum.GetValues(typeof(MiniGames));
      return SelectedMiniGame = (MiniGames)values.GetValue(Random.Range(0, values.Length));
   }
}