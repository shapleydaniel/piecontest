
public static class Displays
{
    public static int numberOfDisplays = 1;

    public static void AdjustNumberOfDisplay(int value)
    {
        if (numberOfDisplays + value <= 0)
        {
            numberOfDisplays = 1;
        }
        else
        {
            numberOfDisplays += value;
        }
    }
    
}
