using TMPro;
using UnityEngine;

public class DisplaySelectionController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI displaySelectionText;

    public void AdjustDisplays(int value)
    {
        Displays.AdjustNumberOfDisplay(value);
        displaySelectionText.text = Displays.numberOfDisplays.ToString("00");
    }
}
