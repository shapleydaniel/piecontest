using System;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public static class Loader
{
    public enum Scene
    {
        Loading = 1,
        MiniGames = 2,
        PlayerSelect = 3,
        Tutorials = 4
    }

    private static Action onLoaderCallback;

    public static void LoadScene(Scene scene)
    {
        //Set Loader callback action to load target scene
        onLoaderCallback = () =>
        {
            SceneManager.LoadScene(scene.ToString());
        };

        //Load the loading scene
        Scores.ClearScores();
        SceneManager.LoadScene(Scene.Loading.ToString());
    }

    public static void LoaderCallback()
    {
        if (onLoaderCallback == null) return;
        onLoaderCallback();
        onLoaderCallback = null;
    }
}