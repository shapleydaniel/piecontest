using UnityEngine;

public class GameDisplay: Display
{
    [Header("Display Cameras")]
    [SerializeField] protected Camera gameDisplayCamera;
    [SerializeField] protected Camera gameUIDisplayCamera;
    [Space]
    [SerializeField] private PlayerController selectedPlayerControllerPrefab;
    [SerializeField] protected Transform instantiatedParentTransform;

    public override void Initialise()
    {
        gameDisplayCamera.targetDisplay = displayID;
        if(displayID > 0)gameDisplayCamera.GetComponent<AudioListener>().enabled = false;
        if(gameUIDisplayCamera != null) gameUIDisplayCamera.targetDisplay = displayID;
        CreateGames();
    }

    protected virtual void CreateGames()
    {
        var controller = Instantiate(selectedPlayerControllerPrefab, instantiatedParentTransform);
        controller.transform.SetAsFirstSibling();
        controller.DisplayNumber = displayID;
        controller.gameObject.name += $" {displayID}";
        controller.Initialise();
    }
}