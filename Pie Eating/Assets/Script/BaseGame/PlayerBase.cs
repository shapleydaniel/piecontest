using Script.Audio;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Script.BaseGame
{
    public class PlayerBase: MonoBehaviour
    {
        [BoxGroup("Required Components")]
        [SerializeField] protected TextMeshProUGUI m_PlayerTitle;
        [BoxGroup("Required Components")]
        [SerializeField] protected Image m_CharacterImage;
        
        [Space] [BoxGroup("Button Events")] 
        [SerializeField] private UnityEvent m_OnPositiveButton;
        [BoxGroup("Button Events")] 
        [SerializeField]private UnityEvent m_OnNegativeButton;

        [HideInInspector]public CharacterData m_PlayerCharacter;
        
        protected int m_Score;
        private InputController m_Inputs;

        private void Start()
        {
            m_Inputs = new InputController($"Player{m_PlayerCharacter.Index}");
        }

        private void Update()
        {
            if (m_PlayerCharacter == null) return;
            ControlTriggered(m_Inputs.ReadInputAxis());
        }

        private void ControlTriggered(float direction)
        {
            if (direction == 0) return;
            if (direction > 0)
            {
                PositiveButton();
            }
            else
            {
                NegativeButton();
            }
        }

        protected virtual void PositiveButton()
        {
            var postiveSfx = m_PlayerCharacter.GetPostiveSFX();
            AudioController.TriggerAudio(postiveSfx.AudioClip, AudioMasters.Sfx, postiveSfx.Pitch);
            m_OnPositiveButton?.Invoke();
        }

        protected virtual void NegativeButton()
        {
            var negativeSfx = m_PlayerCharacter.GetNegativeSFX();
            AudioController.TriggerAudio(negativeSfx.AudioClip, AudioMasters.Sfx, negativeSfx.Pitch);
            m_OnNegativeButton?.Invoke();
        }

        public virtual void SetData(CharacterData player)
        {
            m_PlayerCharacter = player;
            if (m_PlayerTitle != null)
            {
                m_PlayerTitle.text = player.Name;
                m_PlayerTitle.color = player.Color;
            }

            if(m_CharacterImage != null)m_CharacterImage.sprite = player.Sprite;
        }
    }
}