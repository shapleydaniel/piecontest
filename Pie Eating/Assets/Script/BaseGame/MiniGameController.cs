using System;
using System.Collections.Generic;
using MiniGame;
using UnityEngine;

public class MiniGameController : GameDisplay
{
    [Serializable]
    private class Controllers
    {
        public MiniGames m_MiniGame;
        public PlayerController m_PlayerControllerPrefab;
        public GamePlayController m_GamePlayControllerPrefab;
    }

    [Space]
    [Header("Prefabs")]
    [SerializeField] private List<Controllers> miniGameControllersPrefabs;


    protected override void CreateGames()
    {
        var selectedGame = SelectedGameController.SelectedGame?.Invoke();
        if (selectedGame == null)
        {
            Debug.Log($"No Currently selected Game");
            return;
        }
        
        
        Controllers selectedControllerPrefab = miniGameControllersPrefabs.Find(x => x.m_MiniGame == StaticSelectedGame.GetSelectedMiniGame);
        if (selectedControllerPrefab == null)
        {
            Debug.LogError($"No Mini Game Controller for {selectedGame.MiniGames}");
        }

        var controller = Instantiate(selectedControllerPrefab.m_PlayerControllerPrefab, instantiatedParentTransform);
        controller.transform.SetAsFirstSibling();
        controller.DisplayNumber = displayID;
        var o = controller.gameObject;
        o.name = $"{RemoveClone(o.name)} {displayID}";

        if (selectedControllerPrefab.m_GamePlayControllerPrefab != null)
        {
            CreateGame(selectedControllerPrefab.m_GamePlayControllerPrefab);
        }

        controller.Initialise();
    }

    private void CreateGame(GamePlayController gameController)
    {
        var controller = Instantiate(gameController, transform);
        controller.gameObject.name = $"{RemoveClone(controller.gameObject.name)} {displayID}";
    }

    private string RemoveClone(string name)
    {
        return name.Replace("(Clone)", "");
    }
}