using System.Collections;
using System.Linq;
using TMPro;
using UnityEngine;

public class Winner : MonoBehaviour
{
   [SerializeField] private GameObject winnersGroup;
   [SerializeField] private TextMeshProUGUI winnerText;
   [SerializeField] private TextMeshProUGUI playerText;
   [SerializeField] private TextMeshProUGUI scoreText;

   [SerializeField] private int waitForSecondsBeforeExitGame;
   
   private void OnEnable()
   {
      GameStates.EndGame += SortScores;
      winnersGroup.SetActive(false);
   }

   private void OnDisable()
   {
      GameStates.EndGame -= SortScores;
   }

   private void SortScores()
   {
      var topScore = 0f;
      var player = ScriptableObject.CreateInstance<CharacterData>();
      var allScores = Scores.scores;
      foreach (var score in allScores.Where(score => score.Value > topScore))
      {
         player = score.Key;
         topScore = score.Value;
      }

      winnerText.text = $"Player {player.Index+1}";
      playerText.text = player.Name;
      scoreText.text = $"{ConvertScore(topScore)} Whole Pies";
      PresentWinner();
   }

   private string ConvertScore(float score)
   {
      return $"{Mathf.Floor(score)}";
   }

   private void PresentWinner()
   {
      winnersGroup.SetActive(true);
      WinnerConfetti.TriggerParticles?.Invoke();
      StartCoroutine(nameof(ExitGame));
   }

   private IEnumerator ExitGame()
   {
      yield return new WaitForSeconds(waitForSecondsBeforeExitGame);
      GameStates.ExitGame?.Invoke();
   }
}