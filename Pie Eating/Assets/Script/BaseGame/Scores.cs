using System;
using System.Collections.Generic;

public class Scores
{
    public static Dictionary<CharacterData, float> scores = new Dictionary<CharacterData, float>();

    public static Action<CharacterData, float> ScoreChanged;

    public static void AddScore(CharacterData player, float score)
    {
        if (!scores.ContainsKey(player))
        {
            scores.Add(player, score);
            return;
        }
        
        scores[player] = score;
        ScoreChanged?.Invoke(player, score);
    }

    public static void AddScore(CharacterData player)
    {
        if (!scores.ContainsKey(player))
        {
            scores.Add(player, 1);
            ScoreChanged?.Invoke(player, 1);
            return;
        }
        
        scores[player]++;
        ScoreChanged?.Invoke(player, scores[player]);
    }
    
    public static void ClearScores()
    {
        scores.Clear();
    }
    
}
