using System.Collections.Generic;
using System.Linq;
using Script.BaseGame;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    protected List<PlayerBase> m_PlayerUI = new List<PlayerBase>();
    private int m_DisplayNumber;
    
    private const int MAX_NUMBER_PLAYERS_PER_SCREEN = 4;
    
    public int DisplayNumber
    {
        set => m_DisplayNumber = value;
    }

    public virtual void Initialise()
    {
        SelectPlayers();
        GetPlayers();
        ResetPlayers();
        SetActivePlayers();

    }

    private void SetActivePlayers()
    {
        var arrayStartingNumber = m_DisplayNumber * MAX_NUMBER_PLAYERS_PER_SCREEN;
        var selectedPlayers = PlayerDataController.GetSelectedPlayers;
        
        if (selectedPlayers == null || selectedPlayers.Count <= 0)
        {
            Debug.LogError($"No Selected Players");
            return;
        }
            
        foreach (var player in selectedPlayers)
        {
            if (player.Index < arrayStartingNumber ||
                player.Index - arrayStartingNumber >= m_PlayerUI.Count) continue;
            var newPlayer = m_PlayerUI[player.Index - arrayStartingNumber];
            newPlayer.SetData(player);
            newPlayer.gameObject.SetActive(true);
        }
    }

    private void SelectPlayers()
    {
        PlayerDataController.SelectPlayers?.Invoke(MAX_NUMBER_PLAYERS_PER_SCREEN);
    }

    private void GetPlayers()
    {
        m_PlayerUI = GetComponentsInChildren<PlayerBase>(true).ToList();
        
        
        
        /*foreach (Transform child in transform)
        {
            var player = child.GetComponent<PlayerBase>();
            if (player == null) continue;
            DisplayPlayers.Add(player);
        }*/
    }

    private void ResetPlayers()
    {
        foreach (var player in m_PlayerUI)
        {
            player.gameObject.SetActive(false);
        }
    }
}