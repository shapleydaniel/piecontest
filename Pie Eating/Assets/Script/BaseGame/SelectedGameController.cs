using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MiniGame
{
    public class SelectedGameController: MonoBehaviour
    {
        [SerializeField] private GameDataObject[] m_AllAvailableGames;
    
        private GameDataObject m_SelectedGame;

        public static Func<GameDataObject> SelectedGame;

        private void OnEnable()
        {
            SelectedGame += GetCurrentlySelectedGame;
        }

        private void OnDisable()
        {
            SelectedGame -= GetCurrentlySelectedGame;
        }

        private GameDataObject GetCurrentlySelectedGame()
        {
            if (m_SelectedGame == null)
            {
                SelectGame();
            }

            return m_SelectedGame;
        }
    
        private void SelectGame()
        {
            //TODO: Either check before or some how return default game
            if (m_AllAvailableGames == null || m_AllAvailableGames.Length <= 0)
                return;
        
            var randomIndex = Random.Range(0, m_AllAvailableGames.Length);
            var selectedMinigame = m_AllAvailableGames[randomIndex];
            m_SelectedGame = selectedMinigame;
        }
    }
}