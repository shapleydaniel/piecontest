using UnityEngine;

public class Display: MonoBehaviour
{
    public virtual void Initialise(){}

    protected int displayID;

    public int SetDisplayID
    {
        set => displayID = value;
    }
}