using UnityEngine;

public class InputController
{
    private readonly string _inputAxis;

    public InputController(string positiveKeyInput)
    {
        _inputAxis = positiveKeyInput;
    }


    public float ReadInputAxis()
    {
        return Input.GetAxis(_inputAxis);
    }
}