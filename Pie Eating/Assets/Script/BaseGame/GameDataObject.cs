using Tutorials;
using UnityEngine;

namespace MiniGame
{
    [CreateAssetMenu(menuName = "Game/BaseGame", fileName = "Game")]
    public class GameDataObject : ScriptableObject
    {
        [SerializeField] private string m_Title;
        [SerializeField] private MiniGames m_MiniGames;
        [SerializeField] private GameRulesObject m_Rules;

        public string Title => m_Title;
        public MiniGames MiniGames => m_MiniGames;
        public GameRulesObject Rules => m_Rules;

    }
}