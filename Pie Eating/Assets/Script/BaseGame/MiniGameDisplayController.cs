public class MiniGameDisplayController : DisplayController
{
    private void Awake()
    {
        for (var i = 0; i < Displays.numberOfDisplays; i++)
        {
            var miniGameController = (MiniGameController)Instantiate(displayPrefab, transform);
            miniGameController.SetDisplayID = i;
            var o = miniGameController.gameObject;
            o.name = $"{o.name.Replace("(Clone)", "")} {i}";
            miniGameController.Initialise();
        }
    }
}