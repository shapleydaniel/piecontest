using System;
using UnityEngine;

public class GameStates: MonoBehaviour
{
    public static bool gameActive;
    
    public static Action StartGame;
    public static Action EndGame;
    public static Action ExitGame;
    
    private void OnEnable()
    {
        StartGame += () => gameActive = true;
        EndGame += () => gameActive = false;
        ExitGame += LoadMainMenu;
    }

    private void OnDisable()
    {
        StartGame = null;
        EndGame = null;
        ExitGame = null;
    }

    private void LoadMainMenu()
    {
        Loader.LoadScene(Loader.Scene.PlayerSelect);
    }
}
