using System;
using UnityEngine;

public class WinnerConfetti : MonoBehaviour
{
    [SerializeField] private ParticleSystem[] _particleSystems;

    public static Action TriggerParticles;

    private void OnEnable()
    {
        TriggerParticles += OnTriggerParticles;
    }

    private void OnDisable()
    {
        TriggerParticles = null;
    }

    private void OnTriggerParticles()
    {
        foreach (var particles in _particleSystems)
        {
            particles.Play();
        }
    }
}
