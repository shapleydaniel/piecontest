
using System.Linq;

public class EatingPlayersController : PlayerController
{
    private void OnEnable()
    {
        GameStates.EndGame += TurnOffScores;
    }

    private void OnDisable()
    {
        GameStates.EndGame -= TurnOffScores;
    }

    private void TurnOffScores()
    {
        foreach (var player in m_PlayerUI.Cast<EatingPlayer>())
        {
            player.RemoveScore();
        }
    }
}