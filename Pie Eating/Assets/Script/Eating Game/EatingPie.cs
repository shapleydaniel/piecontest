using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class EatingPie : MonoBehaviour
{
    [SerializeField] private Image pieImage;

    [Space][Header("Tween")] 
    [SerializeField] private float duration;
    [SerializeField] private Ease ease;
    
    private RectTransform _rectTrans;

    private void Awake()
    {
        _rectTrans = GetComponent<RectTransform>();
    }

    public void PieEnter(Action OnComplete = null)
    {
        _rectTrans.anchoredPosition = new Vector2(0, 800);
        _rectTrans.DOAnchorPos(Vector2.zero, duration).SetEase(ease).onComplete += () => OnComplete?.Invoke();
    }

    public void SetPieImage(Sprite newPie)
    {
        pieImage.sprite = newPie;
    }
}
