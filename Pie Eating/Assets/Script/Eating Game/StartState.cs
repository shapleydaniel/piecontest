using DG.Tweening;
using TMPro;
using UnityEngine;

public class StartState : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI numberText;
    [SerializeField] private string startText;

    [Space] [Header("Tween Attributes")] 
    [SerializeField] private Color[] colorsText;
    [SerializeField] private float duration;
    [SerializeField] private Ease ease;
    

    private int _countDown = 3;
    
    private void Start()
    {
        numberText.transform.localScale = Vector3.zero;
        numberText.color = colorsText[3];
        var startingSequence = DOTween.Sequence();
        AddTweenToSequence(startingSequence);
        AddTweenToSequence(startingSequence);
        AddTweenToSequence(startingSequence);
        AddTweenToSequence(startingSequence);
        startingSequence.OnComplete(
            () =>
            {
                GameStates.StartGame?.Invoke();
            }
            );
        startingSequence.Play();
    }

    private void AddTweenToSequence(Sequence tweenSequence)
    {
        tweenSequence.Append(numberText.DOScale(1,duration).SetEase(ease));
        tweenSequence.AppendCallback(() =>
        {
            numberText.transform.localScale = Vector3.zero;
            numberText.text = CountDown();
            numberText.color = colorsText[_countDown];
        });
    }

    private string CountDown()
    {
        _countDown--;
        return _countDown <= 0 ? startText : _countDown.ToString();
    }
}
