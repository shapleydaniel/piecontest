using System;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class Timer : MonoBehaviour
{
   [SerializeField] private string endText;
   
   private TextMeshProUGUI _textMesh;

   private float time;
   private int maxTime = 20;

   private void Awake()
   {
      _textMesh = GetComponent<TextMeshProUGUI>();
      time = maxTime;
   }

   private void Update()
   {
      if (!GameStates.gameActive) return;
      time -= Time.deltaTime;
      if (time <= 0)
      {
         GameStates.EndGame?.Invoke();
         _textMesh.text = endText;
         return;
      }
      
      _textMesh.text =  TimeSpan.FromSeconds(time).ToString("ss");
   }
}
