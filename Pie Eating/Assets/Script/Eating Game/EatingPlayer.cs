using Script.BaseGame;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

public class EatingPlayer : PlayerBase
{
    [SerializeField] private TextMeshProUGUI m_ScoreTextMesh;
    [SerializeField] private EatingPie m_Pie;

    private int m_PieSection;
    private bool m_ButtonSet;
    private bool m_Animation;

    private float m_TotalScore;
    
    protected override void NegativeButton()
    {
        if (!m_ButtonSet || m_Animation || !GameStates.gameActive) return;
        m_ButtonSet = false;
        AddScore();
        base.NegativeButton();
    }
    
    protected override void PositiveButton()
    {
        if (m_ButtonSet || m_Animation || !GameStates.gameActive) return;
        m_ButtonSet = true;
        base.PositiveButton();
    }

    private void AddScore()
    {
        m_PieSection++;
        m_TotalScore += 0.25f;
        if (m_PieSection >= 4)
        {
            m_Score = (int)Mathf.Floor(m_TotalScore);
            m_ScoreTextMesh.text = m_Score.ToString("00"); 
            m_PieSection = 0;
            m_Animation = true;
            m_Pie.PieEnter(() => m_Animation = false);
        }
       
        Scores.AddScore(m_PlayerCharacter, m_TotalScore);
    }

    public void RemoveScore()
    {
        m_ScoreTextMesh.gameObject.SetActive(false);
        m_PlayerTitle.gameObject.SetActive(false);
    }
}