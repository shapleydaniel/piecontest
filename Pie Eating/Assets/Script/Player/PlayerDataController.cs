using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerDataController : MonoBehaviour
{
    [SerializeField] private CharacterData[] m_AllAvailablePlayers;

    private static List<CharacterData> m_ActivePlayers = new List<CharacterData>();
    private static List<CharacterData> m_SelectedPlayers = new List<CharacterData>();
    private List<CharacterData> m_CurrentAvailablePlayers = new List<CharacterData>();

    public static Action<CharacterData> AddActivePlayer;
    public static Action<CharacterData> RemoveActivePlayer;
    public static List<CharacterData> GetActivePlayers => m_ActivePlayers;
    public static int NumberActivePlayers => m_ActivePlayers.Count;
    public static List<CharacterData> GetSelectedPlayers => m_SelectedPlayers;
    
    public static Func<int, List<CharacterData>> SelectPlayers;
    
    private void OnEnable()
    {
        AddActivePlayer += AddPlayerToActive;
        RemoveActivePlayer += RemovePlayerFromActive;
        SelectPlayers += SelectAllPlayers;
    }

    private void OnDisable()
    {
        AddActivePlayer -= AddPlayerToActive;
        RemoveActivePlayer -= RemovePlayerFromActive;
        SelectPlayers -= SelectAllPlayers;
    }

    private void Awake()
    {
        m_CurrentAvailablePlayers = m_AllAvailablePlayers.ToList();
    }

    private List<CharacterData> SelectAllPlayers(int maxPlayers)
    {
        for (int i = 0; i < maxPlayers; i++)
        {
            RandomlySelectPlayer(i);
        }

        return m_SelectedPlayers;
    }

    private void RandomlySelectPlayer(int index)
    {
        var player = m_CurrentAvailablePlayers[Random.Range(0, m_CurrentAvailablePlayers.Count)];
        m_CurrentAvailablePlayers.Remove(player);
        player.Index = index;
        m_SelectedPlayers.Add(player);
    }

    private void AddPlayerToActive(CharacterData characterData)
    {
        m_ActivePlayers.Add(characterData);
    }

    private void RemovePlayerFromActive(CharacterData characterData)
    {
        m_ActivePlayers.Remove(characterData);
    }
}