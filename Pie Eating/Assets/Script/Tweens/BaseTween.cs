using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace ComponentTween
{
    /// <summary>
    /// Base functions for all tween components
    /// </summary>
    public abstract class BaseTween: MonoBehaviour
    {
        [BoxGroup("Base Variables")]
        [SerializeField] protected float m_Duration;
        [BoxGroup("Base Variables")]
        [SerializeField] protected Ease m_Ease;
        
        [BoxGroup]
        [SerializeField] private bool m_PlayOnAwake;

        private void Awake()
        {
            if(m_PlayOnAwake) In();
        }

        public abstract void Out(Action OnComplete = null);

        public abstract void In(Action OnComplete = null);

        public abstract void Reset();
    }
}