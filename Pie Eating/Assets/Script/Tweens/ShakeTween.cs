using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace ComponentTween
{
    /// <summary>
    /// Shake object 
    /// </summary>
    public class ShakeTween : BaseTween
    {
        [BoxGroup("Specific Variables")]
        [SerializeField] private Vector3 m_ShakeStrength;
        [BoxGroup("Specific Variables")]
        [SerializeField] private int m_Vibrato;
        [BoxGroup("Specific Variables")]
        [SerializeField] private float m_Randomness;

        public override void Out(Action OnComplete = null)
        {
            Debug.Log($"no out for shaking tween {name}");
        }

        public override void In(Action OnComplete = null)
        {
            transform.DOShakePosition(m_Duration, m_ShakeStrength, m_Vibrato, m_Randomness).SetEase(m_Ease);
        }

        public override void Reset()
        {
        }
    }
}