using System;
using DG.Tweening;
using UnityEngine;

namespace ComponentTween
{
    /// <summary>
    /// Scale an object to pop into screen
    /// </summary>
    public class ScaleTween : BaseTween
    {
        public override void Out(Action OnComplete = null)
        {
            transform.DOScale(0, m_Duration).SetEase(m_Ease).onComplete += () => OnComplete?.Invoke();
        }

        public override void In(Action OnComplete = null)
        {
            transform.DOScale(1, m_Duration).SetEase(m_Ease).onComplete += () => OnComplete?.Invoke();
        }

        public override void Reset()
        {
            transform.localScale = Vector2.zero;
        }
    }
}