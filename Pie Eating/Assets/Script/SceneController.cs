using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    [SerializeField] private string m_StartingScene;
    [SerializeField] private bool m_LoadOnAwake = false;
    
    private const string LoadingScene = "Loading";
    
    private string m_CurrentScene;

    public static Action<string> LoadNewScene;

    private void OnEnable()
    {
        LoadNewScene += LoadScene;
    }

    private void OnDisable()
    {
        LoadNewScene -= LoadScene;
    }

    private void Awake()
    {
        if(string.IsNullOrEmpty(m_StartingScene) || !m_LoadOnAwake)
            return;

        LoadScene(m_StartingScene);
    }

    public void LoadScene(string sceneName)
    {
        if (string.IsNullOrEmpty(sceneName))
        {
            Debug.LogError($"Missing Scene Name to Load: {sceneName}");
            return;
        }
        
        StartCoroutine(LoadSceneCoroutine(sceneName));
    }
    
    private IEnumerator LoadSceneCoroutine(string sceneName)
    {
        LoadLoadingScene();
        if(m_CurrentScene != null) UnLoadScene(m_CurrentScene);
        var r = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        yield return new WaitUntil(() => r.isDone);
        m_CurrentScene = sceneName;
        UnloadLoadingScene();
    }

    private void UnLoadScene(string scene)
    {
        if (string.IsNullOrEmpty(scene))
            return;
        SceneManager.UnloadSceneAsync(scene);
    }

    private void LoadLoadingScene()
    {
        SceneManager.LoadScene(LoadingScene, LoadSceneMode.Additive);
    }

    private void UnloadLoadingScene()
    {
        SceneManager.UnloadSceneAsync(LoadingScene);
    }
}