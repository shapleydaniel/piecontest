using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectPool<T> where T : Component
{
    public List<T> PooledObjects;
    private readonly GameObject objectToPool;
    private readonly Transform parentTransform;
    private readonly int amountToPool;
    private readonly int maximumNumberOfObjects;

    public ObjectPool(GameObject objectToPool, int amountToPool, Transform parentTransform, int maxObjects = -1)
    {
        this.objectToPool = objectToPool;
        this.amountToPool = amountToPool;
        this.parentTransform = parentTransform;
        maximumNumberOfObjects = maxObjects;
    }

    public void CreatePool()
    {
        PooledObjects = new List<T>();
        for(var i = 0; i < amountToPool; i++)
        {
            CreateNewInstance(i);
        }
    }

    public T CreateNewInstance()
    {
        var tmp = CreateInstance();
        tmp.name += $" {amountToPool+1}";
        tmp.SetActive(false);
        var component = tmp.GetComponent<T>();
        PooledObjects.Add(component);
        return component;
    }

    public T GetFreeObject()
    {
        foreach (var pooledObject in PooledObjects.Where(pooledObject => !pooledObject.gameObject.activeSelf))
        {
            pooledObject.gameObject.SetActive(true);
            return pooledObject;
        }

        if (maximumNumberOfObjects > -1 && maximumNumberOfObjects >= PooledObjects.Count) return null;
        var newObject = CreateNewInstance();
        newObject.gameObject.SetActive(true);
        return newObject;
    }

    public List<T> GetAllActiveObjects()
    {
        return PooledObjects.Where(pooledObject => pooledObject.gameObject.activeSelf).ToList();
    }

    public void ResetPool()
    {
        foreach (var tComponent in PooledObjects)
        {
            tComponent.gameObject.SetActive(false);
        }
    }

    public void SetAllObjectsActive(bool active)
    {
        foreach (var pooledObject in PooledObjects)
        {
            pooledObject.gameObject.SetActive(active);
        }
    }

    private GameObject CreateInstance()
    {
        return Object.Instantiate(objectToPool, parentTransform);
    }

    protected virtual T CreateNewInstance(int number)
    {
        var tmp = CreateInstance();
        tmp.name += $" {number}";
        tmp.SetActive(false);
        var component = tmp.GetComponent<T>();
        PooledObjects.Add(component);
        return component;
    }
}