﻿using TMPro;
using UnityEngine;

public class CountDown : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI timerText;
    [SerializeField] private TextMeshProUGUI titleText;
    [SerializeField] private GameObject textGroup;

    [Space]
    [SerializeField] private string[] titleStrings;

    [SerializeField] private string endTitleString;

    [Space]
    [SerializeField] private int maxTime;

    private float _timer;

    private void Start()
    {
        DeactivateTimer();
    }

    private void Update()
    {
        var active = PlayerDataController.NumberActivePlayers > 1;
        
        switch (active)
        {
            case true when !textGroup.activeSelf:
                ResetTimer();
                break;
            case true when _timer >= 0:
            {
                _timer -= Time.deltaTime;
                if (_timer <= 0)
                {
                    //Start Game;
                    timerText.text = endTitleString;
                    titleText.text = "";

                    //TODO Temp
                    Loader.LoadScene(Loader.Scene.Tutorials);

                    return;
                }

                timerText.text = _timer.ToString("00");
                break;
            }
            case false when textGroup.activeSelf:
                DeactivateTimer();
                break;
        }
    }

    private void ResetTimer()
    {
        _timer = maxTime;
        timerText.text = _timer.ToString("00");
        titleText.text = titleStrings[0];
        textGroup.SetActive(true);
    }

    private void DeactivateTimer()
    {
        textGroup.SetActive(false);
    }
}