﻿using ComponentTween;
using Script.BaseGame;
using UnityEngine;

[RequireComponent(typeof(BaseTween))]
public class PlayerSelect : PlayerBase
{
    [Space]
    [SerializeField] private Color disableColor;

    private BaseTween m_Tween;
    
    private void Awake()
    {
        m_Tween = GetComponent<BaseTween>();
    }

    public override void SetData(CharacterData player)
    {
        base.SetData(player);
        m_CharacterImage.color = disableColor;
        m_PlayerTitle.gameObject.SetActive(false);
    }

    protected override void NegativeButton()
    {
        if (!m_PlayerTitle.gameObject.activeSelf) return;
        m_CharacterImage.color = disableColor;
        m_PlayerTitle.gameObject.SetActive(false);
        PlayerDataController.RemoveActivePlayer?.Invoke(m_PlayerCharacter);
        base.NegativeButton();
    }

    protected override void PositiveButton()
    {
        if (m_PlayerTitle.gameObject.activeSelf) return;
        m_CharacterImage.color = Color.white;
        m_PlayerTitle.gameObject.SetActive(true);
        m_Tween.In();
        PlayerDataController.AddActivePlayer?.Invoke(m_PlayerCharacter);
        base.PositiveButton();
    }
}