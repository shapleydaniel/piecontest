﻿using UnityEngine;

[CreateAssetMenu(menuName = "PlayerData")]
public class CharacterData: ScriptableObject
{
    [SerializeField] private string m_Name;
    [SerializeField] private Color m_Color;
    [SerializeField] private Sprite m_Sprite;
    [SerializeField] private Sprite[] m_PieSprites;
    [SerializeField] private Audio.Audio[] m_PostiveSFX;
    [SerializeField] private Audio.Audio[] m_NegativeSFX;
    [HideInInspector] public int Index;
    
    public string Name => m_Name;

    public Sprite Sprite => m_Sprite;
    public Sprite[] PieSprites => m_PieSprites;
    public Color Color => m_Color;

    public Audio.Audio GetPostiveSFX()
    {
        return GetSFX(m_PostiveSFX);
    }

    public Audio.Audio GetNegativeSFX()
    {
        return GetSFX(m_NegativeSFX);
    }

    private Audio.Audio GetSFX(Audio.Audio[] sfx)
    {
        if (sfx.Length > 0) 
            return sfx[Random.Range(0, sfx.Length)];
        
        Debug.LogError($"Missing SFX in {name}");
        return default;
    }
}