using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class InvadingPieController : GamePlayController
{
    [SerializeField] private InvadingPie[] pies;
    [SerializeField] private float maxSpeed;
    [SerializeField] private float maxDelay;

    private float currentYPosition = -135f;

    private void OnEnable()
    {
        GameStates.StartGame += RandomlySelectPies;
    }

    private void RandomlySelectPies()
    {
        var selectedPies = SelectRandomPies();
        Debug.Log($"Number of Pies: {selectedPies.Count}");
        foreach (var pie in selectedPies)
        {
            var reverse = Random.value > 0.5f;
            var delay = Random.Range(0, maxDelay);
            Debug.Log($"Name of Pie: {pie.name}, Reverse: {reverse}, Delay: {delay}");

            //TODO TOO many magic numbers
            pie.TriggerPie(400, maxSpeed, currentYPosition + Random.Range(0, 2)*250, false, delay);
        }
    }

    private List<InvadingPie> SelectRandomPies()
    {
        var numberPies = Random.Range(1, pies.Length);
        var availablePies = pies.ToList();
        var selectedPies = new List<InvadingPie>();
        for (var i = 0; i < numberPies; i++)
        {
            var randomPie = Random.Range(0, availablePies.Count);
            var selectedPie = availablePies[randomPie];
            selectedPies.Add(selectedPie);
            availablePies.Remove(selectedPie);
        }

        return selectedPies;
    }
}