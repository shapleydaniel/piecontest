
using System;
using Random = UnityEngine.Random;

public class InvadingPlayerController : PlayerController
{
    private int _activePlayerIndex;
    private InvadingPlayer _currentPlayer;

    public static Func<float> GetRandomPosition;

    private void OnEnable()
    {
        GetRandomPosition += GetPlayerPosition;
    }

    private void OnDisable()
    {
        GetRandomPosition = null;
    }

    private void Update()
    {
        CheckPlayers();
    }

    private void CheckPlayers()
    {
        var minPie = InvadingPie.GetPieMinXPosition;
        var maxPie = InvadingPie.GetPieMaxXPosition;
        foreach (var playerBase in m_PlayerUI)
        {
            var player = (InvadingPlayer)playerBase;
            var minPosition = player.GetMinXPosition;
            var maxPosition = player.GetMaxXPosition;
            player.SetActive(minPie < maxPosition && maxPie > minPosition);
        }
    }

    private float GetPlayerPosition()
    {
        var player = (InvadingPlayer)m_PlayerUI[Random.Range(0, m_PlayerUI.Count)];
        return player.GetXPosition;
    }
}