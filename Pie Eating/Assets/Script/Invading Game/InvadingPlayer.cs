using Script.BaseGame;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

public class InvadingPlayer : PlayerBase
{
    [SerializeField] private TextMeshProUGUI m_ScoreTextMesh;
    
    [HideInInspector]public bool Active;
   
    private float m_PlayerPosition;
    private float m_Width;
    private bool m_SetButton;

    public float GetXPosition => m_PlayerPosition;
    public float GetMinXPosition => m_PlayerPosition - (m_Width/2);
    public float GetMaxXPosition => m_PlayerPosition + (m_Width/2);
    
    private void Start()
    {
        var rect = GetComponent<RectTransform>();
        m_PlayerPosition = rect.anchoredPosition.x;
        m_Width = rect.rect.width;
    }

    protected override void PositiveButton()
    {
        if (!GameStates.gameActive || !m_SetButton) return;
        m_SetButton = false;
        if (Active)
        {
            m_Score++;
        }
        else
        {
            m_Score--;
        }
        
        Scores.AddScore(m_PlayerCharacter, m_Score);
    }

    protected override void NegativeButton()
    {
        if (!GameStates.gameActive || m_SetButton) return;
        m_SetButton = true;
    }

    
    //FOR TESTING
    public void SetActive(bool active)
    {
        Active = active;
        m_ScoreTextMesh.color = active ? Color.red : Color.white;
    }
}