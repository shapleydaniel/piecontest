using DG.Tweening;
using UnityEngine;
using static InvadingPlayerController;

public class InvadingPie : MonoBehaviour
{
    //TODO REMOVE all magic numbers;
    
    [SerializeField] private float duration;
    [SerializeField] private Ease ease;
    [SerializeField] private float startingPosition;
    [SerializeField] private float endPosition;
    
    private static RectTransform _rectTransform;
    private static float _width;

    public static float GetPieMinXPosition => _rectTransform.anchoredPosition.x - _width;
    public static float GetPieMaxXPosition => _rectTransform.anchoredPosition.x + _width;

    private void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        _width = _rectTransform.rect.width/2;
        SetXPosition(startingPosition);
    }

    public void TriggerPie(float size ,float speed ,float yPosition, bool reverse = false, float delay = 0)
    {
        SetYPosition(yPosition);
        SetSize(size);
        MovePie(reverse, speed, delay);
    }
    
    private void MovePie(bool reverse, float speed ,float delay = 0)
    {
        float finalPosition;
        if (!reverse)
        {
            SetXPosition(startingPosition);
            finalPosition = endPosition;
        }
        else
        {
            SetXPosition(endPosition);
            finalPosition = startingPosition;
        }
        
        _rectTransform.DOAnchorPosX(finalPosition, speed).SetEase(ease).SetDelay(delay);
    }

    private void NewStartingPosition()
    {
        var xPosition = GetRandomPosition?.Invoke() ?? startingPosition;
        SetXPosition(xPosition - _width);
        var yPosition = _rectTransform.anchoredPosition.y;
        yPosition -= 100;
        SetYPosition(yPosition);
    }
    
    private void SetXPosition(float adjustedValue)
    {
        var anchoredPosition = _rectTransform.anchoredPosition;
        anchoredPosition.x = adjustedValue;
        _rectTransform.anchoredPosition = anchoredPosition;
    }

    private void SetYPosition(float adjustedValue)
    {
        var anchoredPosition = _rectTransform.anchoredPosition;
        anchoredPosition.y = adjustedValue;
        _rectTransform.anchoredPosition = anchoredPosition;
    }

    private void ReduceSize(float reduction)
    {
        _rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _rectTransform.rect.width-reduction);
    }

    private void SetSize(float newSize)
    {
        _rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, newSize);
    }
}
