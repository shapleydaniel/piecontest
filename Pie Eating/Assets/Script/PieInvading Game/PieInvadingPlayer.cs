using Script.BaseGame;
using UnityEngine;

public class PieInvadingPlayer : PlayerBase
{
    [SerializeField] private Weapon ammoPrefab;
    
    private bool buttonSet;
    
    protected override void PositiveButton()
    {
        if (!buttonSet) return;
        buttonSet = false;
        FireWeapon();
    }

    protected override void NegativeButton()
    {
        if (buttonSet) return;
        buttonSet = true;
    }

    private void FireWeapon()
    {
        var ammo = Instantiate(ammoPrefab, gameObject.transform.parent);
    }
}
