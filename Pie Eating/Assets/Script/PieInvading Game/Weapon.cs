using System;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private float speed;
    
    private CharacterData player;

    public CharacterData SetPlayer
    {
        set => player = value;
    }
    
    private void Update()
    {
        transform.Translate(0,speed*Time.deltaTime, 0);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            var pie = other.GetComponent<PieInvadingPie>();
            Scores.AddScore(player, pie.scoreValue);
            Destroy(gameObject);
        }

        if (other.CompareTag("Finish"))
        {
            Destroy(gameObject);
        }
    }
}
