using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Script.Audio
{
    public enum AudioMasters
    {
        Sfx = 1,
        Music = 2,
        VO = 3
    }

    public class AudioController : MonoBehaviour
    {
        private readonly string m_MasterMixerName = "Master";
        private static Transform m_parent;
        private static AudioMixer m_masterMixer;
        private static readonly List<AudioSource> m_AudioPool = new List<AudioSource>();
        private static AudioController m_instance;
        private const int MIN_AUDIO_POOL = 4;

        [RuntimeInitializeOnLoadMethod( RuntimeInitializeLoadType.BeforeSceneLoad )]
        static void InitializeType ()
        {
            m_instance = new GameObject($"#{nameof(AudioController)}").AddComponent<AudioController>();
            DontDestroyOnLoad(m_instance );
        }

        private void Awake()
        {
            m_parent = transform;
            m_masterMixer = Resources.Load(m_MasterMixerName) as AudioMixer;
            if(m_AudioPool.Count < 3) CreateAudioPool();
        }

        private void CreateAudioPool()
        {
            for (var i = 1; i < MIN_AUDIO_POOL; i++)
            {
                var currentMixerGroup = (AudioMasters)i;
                var newAudioSource = CreateAudioSource(currentMixerGroup);
                m_AudioPool.Add(newAudioSource);
            }
        }

        public static void TriggerAudio(AudioClip clip, AudioMasters masters, float pitch = 1, Action onComplete = null)
        {
            if (clip == default) 
                return;
            
            var audioSource = FindAvailableAudioSource(masters);
            audioSource.pitch = pitch;
            audioSource.clip = clip;
            audioSource.Play();
            if(onComplete != null)CoroutineController.Start(WaitForAudioToFinish(clip.length, onComplete));

        }

        private static IEnumerator WaitForAudioToFinish(float waitTime, Action onComplete)
        {
            yield return new WaitForSeconds(waitTime);
            onComplete?.Invoke();
        }

        private static AudioSource CreateAudioSource(AudioMasters master)
        {
            var newGameObject = new GameObject
            {
                transform =
                {
                    parent = m_parent
                },
                name = $"AudioSource {master}"
            };

            var newAudioSource =  newGameObject.AddComponent<AudioSource>();
            SetAudioMixerSource(newAudioSource, master);
            return newAudioSource;
        }

        private static AudioSource FindAvailableAudioSource(AudioMasters master)
        {
            foreach (var audioSource in m_AudioPool)
            {
                if(audioSource.isPlaying) continue;
                SetAudioMixerSource(audioSource, master);
                return audioSource;
            }

            return CreateAudioSource(master);
        }

        private static void SetAudioMixerSource(AudioSource audioSource,AudioMasters master)
        {
            var mixerGroup = FindMixerGroup(master.ToString());
            if (mixerGroup != default) audioSource.outputAudioMixerGroup = mixerGroup;
        }

        private static AudioMixerGroup FindMixerGroup(string mixerName)
        {
            var mixer = m_masterMixer.FindMatchingGroups(mixerName);
            if (mixer != null && mixer.Length > 0) return mixer[0];
            Debug.LogError($"Missing Mixer Name: {mixerName}");
            return null;
        }
    }
}