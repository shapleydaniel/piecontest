using System;
using Script.Audio;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Audio
{
    [Serializable]
    public struct Audio
    {
        public AudioClip AudioClip;
        public AudioMasters Master;
        public float Pitch;
    }

    /// <summary>
    /// Use to trigger of single audio shots
    /// </summary>
    public class AudioTriggerComponent : MonoBehaviour
    {
        [SerializeField] private Audio[] clips;

        public void PlayRandomClip()
        {
            int randomSelection = Random.Range(0, clips.Length);
            Play(randomSelection);
        }
        
        public void Play(int clipNumber = 0)
        {
            if (!CheckClips()) return;
            var clip = clips[0];
            AudioController.TriggerAudio(clip.AudioClip, clip.Master, clip.Pitch);
        }

        public void Play(string clipName)
        {
            if (!CheckClips()) return;
            var clip = FindAudio(clipName);
            if (clip.AudioClip == null) return;
            AudioController.TriggerAudio(clip.AudioClip, clip.Master, clip.Pitch);
        }

        private bool CheckClips(int clipNumber = 1)
        {
            if (clips.Length > 0 && clipNumber <= clips.Length) return true;
            Debug.LogError($"Missing clips from {gameObject.name}");
            return false;
        }

        private Audio FindAudio(string clipName)
        {
            foreach (var variableAudio in clips)
            {
                if (String.CompareOrdinal(clipName, variableAudio.AudioClip.name) == 0)
                {
                    return variableAudio;
                }
            }

            return default;
        }
    }
}