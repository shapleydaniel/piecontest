using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class PouringImage : MonoBehaviour
{
    [SerializeField] private float duration;
    
    private Image pouringImage;

    private void Start()
    {
        pouringImage = GetComponent<Image>();
    }

    public void StartPouring()
    {
        pouringImage.fillOrigin = 1;
        pouringImage.fillAmount = 0;
        pouringImage.DOFillAmount(1, duration);
    }

    public void EndPouring()
    {
        pouringImage.fillOrigin = 0;
        pouringImage.fillAmount = 1;
        pouringImage.DOFillAmount(0, duration);
    }
}
