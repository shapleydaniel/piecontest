using Script.BaseGame;
using UnityEngine;
using UnityEngine.UI;

public class FillingPlayer : PlayerBase
{
    [SerializeField] private Image fillingImage;
    [SerializeField] private PouringImage pourImage;
    
    private float fillAmount;

    private bool buttonSet;

    private bool activateCountDown;
    private float pouringCountDown;
    private float maxCountDown = 5;

    private float maxReduceTime = 0.5f;
    private float reducingCountDown;

    private void Start()
    {
        fillingImage.fillAmount = 0f;
        reducingCountDown = maxReduceTime;
    }

    protected override void PositiveButton()
    {
        if (!buttonSet) return;
        buttonSet = false;
        pourImage.StartPouring();
        fillingImage.fillAmount += 0.08f;
        activateCountDown = true;
        pouringCountDown = maxCountDown;
    }

    protected override void NegativeButton()
    {
        if (buttonSet) return;
        buttonSet = true;
    }

    private void FillingStopped()
    {
        if (!activateCountDown) return;
        pouringCountDown -= Time.deltaTime;
        if (!(pouringCountDown <= 0)) return;
        pouringCountDown = maxCountDown;
        pourImage.EndPouring();
        activateCountDown = false;
    }

    private void ReduceFilling()
    {
        reducingCountDown -= Time.deltaTime;
        if (!(reducingCountDown <= 0)) return;
        reducingCountDown= maxReduceTime;
        fillingImage.fillAmount -= 0.05f;
    }
    
    private void FixedUpdate()
    {
        ReduceFilling();
        FillingStopped();
    }
}
