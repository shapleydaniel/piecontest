using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MasterReactionGameController : MonoBehaviour
{
    [SerializeField] private float minTime;
    [SerializeField] private float maxTime;

    public static Action<CharacterData> reactionPlayer;
    public static Action<ReactionController> addController;

    private readonly List<ReactionController> _controllers = new List<ReactionController>();

    private ReactionController _currentReactionController;

    public static bool pieActive;
    private bool _isFirstPlayer = true;
    private int _numberOfChances;
    private bool _master;

    private void OnEnable()
    {
        addController += AddController;
        reactionPlayer += PlayerReaction;
    }

    private void OnDisable()
    {
        addController = null;
        reactionPlayer = null;
    }

    private void AddController(ReactionController controller)
    {
        if (!_master) return;
        _controllers.Add(controller);
    }

    private void PlayerReaction(CharacterData playerIndex)
    {
        if (!_isFirstPlayer || !pieActive) return;
        _isFirstPlayer = false;
        Scores.AddScore(playerIndex);

        _currentReactionController.ResetPie();

        _numberOfChances--;
        if (_numberOfChances <= 0)
        {
            GameStates.EndGame?.Invoke();
            return;
        }

        ResetPie();
        StartCoroutine(nameof(ActivatePieCoroutine));
    }

    private IEnumerator ActivatePieCoroutine()
    {
        yield return new WaitForSeconds(Random.Range(minTime, maxTime));
        ActivatePie();
    }

    private void ActivatePie()
    {
        pieActive = true;
        _currentReactionController = _controllers[Random.Range(0, _controllers.Count)];
        _currentReactionController.ActivatePie();
    }

    private void ResetPie()
    {
        pieActive = false;
        _isFirstPlayer = true;
    }

}