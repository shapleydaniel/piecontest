using System;
using UnityEngine;

public class ReactionController : GamePlayController
{
   [SerializeField] private ReactionPie piePrefab;

   public bool pieActive;

   private void Start()
   {
      MasterReactionGameController.addController?.Invoke(this);
   }

   public void ActivatePie()
   {
      pieActive = true;
      piePrefab.TriggerPie();
   }

   public void ResetPie()
   {
      pieActive = false;
      piePrefab.ResetPie();
   }
}