using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class ReactionPie : MonoBehaviour
{
    [SerializeField] private RectTransform pieImage;
    [SerializeField] private float duration;
    [SerializeField] private Ease ease;
    
    private Tween _pieScaleTween;
    private RectTransform _rectTransform;

    private void OnEnable()
    {
        _rectTransform = GetComponent<RectTransform>();
        ResetPie();
    }

    public void TriggerPie()
    {
        pieImage.localPosition = new Vector3(Random.Range(0, _rectTransform.rect.width), Random.Range(0, _rectTransform.rect.height));
        if (_pieScaleTween == null)
        {
            pieImage.DOScale(Vector3.one, duration).SetEase(ease);
        }
        else
        {
            _pieScaleTween.Play();
        }
    }

    public void ResetPie()
    {
        if(_pieScaleTween != null && _pieScaleTween.IsPlaying()) _pieScaleTween.Kill();
        pieImage.localScale = Vector3.zero;
    }
}
