using Script.BaseGame;
using TMPro;
using UnityEngine;

public class ReactionPlayer : PlayerBase
{
    [SerializeField] private TextMeshProUGUI m_ScoreTextMesh;
    
    private bool m_ButtonSet;

    private void OnEnable()
    {
        Scores.ScoreChanged += AdjustScore;
    }

    private void OnDisable()
    {
        Scores.ScoreChanged -= AdjustScore;
    }

    protected override void PositiveButton()
    {
        if (m_ButtonSet || !MasterReactionGameController.pieActive || !GameStates.gameActive) return;
        m_ButtonSet = true;
        MasterReactionGameController.reactionPlayer?.Invoke(m_PlayerCharacter);
        base.PositiveButton();
    }

    protected override void NegativeButton()
    {
        if (!m_ButtonSet || !MasterReactionGameController.pieActive || !GameStates.gameActive) return;
        m_ButtonSet = false;
        base.NegativeButton();
    }

    private void AdjustScore(CharacterData player, float newScore)
    {
        if (player.Index != m_PlayerCharacter.Index) return;
        m_ScoreTextMesh.text = newScore.ToString("00");
    }
}